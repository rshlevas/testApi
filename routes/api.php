<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/hello', 'Api\TestController@index')->middleware('auth:api');

//FileController routes
Route::post('/files', 'Api\FileController@store');
Route::get('/files', 'Api\FileController@index');
Route::delete('/files/{file}', 'Api\FileController@destroy')->where('file', '[0-9]+');
Route::get('/files/{file}', 'Api\FileController@show')->where('file', '[0-9]+');
Route::get('/files/{file}/download', 'Api\FileController@download')->where('file', '[0-9]+');

// ImageController routes
Route::post('/images', 'Api\ImageController@store');
Route::get('/images', 'Api\ImageController@index');
Route::delete('/images/{image}', 'Api\ImageController@destroy')->where('image', '[0-9]+');
Route::get('/images/{image}', 'Api\ImageController@show')->where('image', '[0-9]+');
Route::get('/images/{image}/download', 'Api\ImageController@download')->where('image', '[0-9]+');