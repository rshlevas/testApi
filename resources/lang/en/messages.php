<?php

return [
    'file_created' => 'File was created successfully',
    'file_delete' => 'File was deleted successfully!',
    'file_delete_error' => 'Can\'t delete file. Try again later',
    'file_save_error' => 'Can\'t save file. Try later please',
];
