<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 11:48
 */

namespace App\Http\Middleware;

use App\Services\RelationFilterService;
use Closure;
use Illuminate\Support\Facades\Auth;

class ImageRelationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checker = new RelationFilterService();
        if (!$checker->checkUserAndModelRelations($request->route('image'), Auth::user())) {
            return redirect('/home');
        }
        return $next($request);
    }

}