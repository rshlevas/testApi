<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 14:52
 */

namespace App\Http\Middleware;

use Closure;
use App\Services\RelationFilterService;
use Illuminate\Support\Facades\Auth;

class FileRelationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checker = new RelationFilterService();
        if (!$checker->checkUserAndModelRelations($request->route('file'), Auth::user())) {
            return redirect('/home');
        }
        return $next($request);
    }
}