<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 10:01
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImageUploadRequest;
use App\Image;
use App\Services\FileManager\ImageManager;
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{
    /**
     * ImageController constructor.
     */
    public function __construct()
    {
       $this->middleware('auth:api');
       $this->middleware('relation.image')->only(['destroy', 'show', 'download']);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = Auth::user();
        return response()->json(['images' => $user->images], 200);
    }

    /**
     * @param ImageUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ImageUploadRequest $request)
    {
        $user = Auth::user();
        $manager = new ImageManager(new Image);
        $result = $manager->create($request, $user);

        if (!$result) {
            return response()->json(__('messages.file_save_error'));
        }
        return response()->json(__('messages.file_created'), 201);

    }

    /**
     * @param Image $image
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Image $image)
    {
        if ($image->delete()) {
            return response()->json(__('messages.file_delete'), 204);
        }
        return response()->json(__('messages.file_delete_error'), 200);
    }

    /**
     * Action for showing single image
     *
     * @param Image $image
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Image $image)
    {
        return response()->json(['image' => $image], 200);
    }

    /**
     * Action for image downloading
     *
     * @param Image $image
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Image $image)
    {
        $file = public_path(). "/storage/images/" . $image->name;
        $type = explode('.', $image->name);
        $headers = [
            'Content-Type: image/'.$type[1],
        ];
        return response()->download($file, $image->name, $headers);
    }
}