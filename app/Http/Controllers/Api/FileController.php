<?php

namespace App\Http\Controllers\Api;

use App\File;
use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Services\FileManager\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    /**
     * FileController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('relation.file')->only(['destroy', 'show', 'download']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return response()->json(['files' => $user->files], 200);
    }

    /**
     * Create new file
     *
     * @param FileUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FileUploadRequest $request)
    {
        $user = Auth::user();

        $manager = new FileManager(new File);
        $result = $manager->create($request, $user);

        if (!$result) {
            return response()->json(__('messages.file_save_error'), 200);
        }
        return response()->json(__('messages.file_created'), 201);
    }

    /**
     * Show resource
     *
     * @param File $file
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(File $file)
    {
        return response()->json(['image' => $file], 200);
    }

    /**
     * Delete resource
     *
     * @param File $file
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(File $file)
    {
        if ($file->delete()) {
            return response()->json(__('messages.file_delete'), 204);
        }
        return response()->json(__('messages.file_delete_error'), 200);
    }

    /**
     * File downloading
     *
     * @param File $file
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(File $file)
    {
        $downloadFile = public_path(). "/storage/files/" . $file->name;
        $type = explode('.', $file->name);
        switch($type[1]) {
            case 'txt':
                $content = 'text/plain';
                break;
            case 'pdf':
                $content = 'application/pdf';
                break;
        }
        $headers = [
            'Content-Type: '. $content,
        ];
        return response()->download($downloadFile, $file->title, $headers);
    }
}
