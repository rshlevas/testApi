<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 13.11.2017
 * Time: 17:37
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index()
    {
        return response()->json([
            'message' => 'Hello'
        ], 200);
    }
}