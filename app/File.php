<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 13.11.2017
 * Time: 19:16
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    /**
     * Related user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Only pdf files
     *
     * @param $query
     * @return mixed
     */
    public function scopePdf($query)
    {
        return $query->where('type', 1);
    }

    /**
     * Only text files
     *
     * @param $query
     * @return mixed
     */
    public function scopeText($query)
    {
        return $query->where('type', 2);
    }

    /**
     * Delete image from db and storage dir
     *
     * @return bool|null
     */
    public function delete()
    {
        if (Storage::delete($this->path)) {
            return parent::delete();
        }
        return false;

    }

}