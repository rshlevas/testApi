<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 11:26
 */

namespace App\Services;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RelationFilterService
{
    /**
     * Function, that checked if user is owner of the object
     *
     * @param Model $model
     * @param User $user
     * @return bool
     */
    public function checkUserAndModelRelations(Model $model, User $user): bool
    {
        if ($model->user_id == $user->id) {
            return true;
        }
        return false;
    }

    /**
     * Function, that checked if user is owner of the object
     *
     * @param int $id
     * @param string $type
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function checkUserRelations(int $id, string $type, User $user): bool
    {
        if (!method_exists($user, $type)) {
            throw new \Exception('Method: ' . $type . ' don\'t exist in User object');
        }
        return $this->checker($id, $user, $type);
    }

    /**
     * Function. that checks if object with some id related to Model object
     *
     * @param int $id
     * @param Model $model
     * @param string $type
     * @return bool
     */
    protected function checker(int $id, Model $model, string $type) : bool
    {
        $collection = $model->{$type};
        foreach ($collection as $item) {
            if ($item->id == $id) {
                return true;
            }
        }
        return false;
    }


}