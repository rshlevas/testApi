<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 16:02
 */

namespace App\Services\FileManager;


use App\Image;

class ImageManager extends BaseManager
{
    public function __construct(Image $image)
    {
        parent::__construct($image);
    }

    protected function getStoreDir()
    {
        return 'public/images';
    }

    protected function getType()
    {
        return 'image';
    }

    protected function prepare($request)
    {
        if ($request->input('avatar')) {
            $this->model->is_avatar = 1;
        }
        $this->model->description = $request->input('description');
        return true;
    }
}