<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 16:02
 */

namespace App\Services\FileManager;


use App\File;

class FileManager extends BaseManager
{
    public function __construct(File $file)
    {
        parent::__construct($file);
    }

    protected function getStoreDir()
    {
        return 'public/files';
    }

    protected function getType()
    {
        return 'file';
    }

    protected function prepare($request)
    {
        $this->model->title = $request->input('title');
        $this->model->description = $request->input('description');
        $type = explode('.', $this->model->name);
        switch ($type[1]) {
            case 'pdf':
                $this->model->type = 1;
                break;
            case 'txt':
                $this->model->type = 2;
                break;
        }
    }
}