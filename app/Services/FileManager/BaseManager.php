<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 15:13
 */

namespace App\Services\FileManager;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

abstract class BaseManager
{
    /**
     * @var $storeDir <p>Directory to store files</p>
     */
    protected $storeDir;

    /**
     * @var $type <p>Type of file(image, file etc)</p>
     */
    protected $type;

    /**
     * @var Model <p>Active record of file class</p>
     */
    protected $model;

    /**
     * BaseManager constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->type = $this->getType();
        $this->storeDir = $this->getStoreDir();
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    abstract protected function getStoreDir();

    /**
     * @return mixed
     */
    abstract protected function getType();

    /**
     * @param $file
     * @return mixed
     */
    protected function getPath($file)
    {
        return $file->store($this->storeDir);
    }

    /**
     * @param $file
     * @return mixed
     */
    protected function getName($file)
    {
        return $file->hashName();
    }

    /**
     * @return bool
     */
    public function save()
    {
        return $this->model->save();
    }

    /**
     * @param FormRequest $request
     * @param User $user
     * @return bool|Model
     */
    public function create(FormRequest $request, User $user)
    {
        $file = $request->file($this->type);
        $path = $this->getPath($file);
        $name = $this->getName($file);
        if (!$path || !$name) {
            return false;
        }
        $this->model->path = $path;
        $this->model->name = $name;
        $this->model->user_id = $user->id;
        $this->prepare($request);
        if (!$this->save()) {
            $this->deleteFromDir();
            return false;
        }
        return $this->model;
    }

    /**
     * @param $user
     * @param $request
     * @return mixed
     */
    abstract protected function prepare($request);

    /**
     * @return mixed
     */
    protected function deleteFromDir()
    {
        return Storage::delete($this->model->path);
    }
}