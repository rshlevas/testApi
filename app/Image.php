<?php
/**
 * Created by PhpStorm.
 * User: Leva
 * Date: 14.11.2017
 * Time: 9:56
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    /**
     * Related user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Only avatar image
     *
     * @param $query
     * @return mixed
     */
    public function scopeavatar($query)
    {
        return $query->where('is_avatar', 1);
    }

    /**
     * Delete image from db and storage dir
     *
     * @return bool|null
     */
    public function delete()
    {
        if (Storage::delete($this->path)) {
            return parent::delete();
        }
        return false;

    }
}